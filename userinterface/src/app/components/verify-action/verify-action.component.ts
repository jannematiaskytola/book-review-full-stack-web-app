import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { book } from 'src/app/models/book.model';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';


@Component({
  selector: 'app-verify-action',
  templateUrl: './verify-action.component.html',
  styleUrls: ['./verify-action.component.scss']
})
export class VerifyActionComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  public book:book={
    title: '',
    price: 0,
    released: '',
    score: 0,
    id: ''
  }
  constructor(   private snackBar: MatSnackBar,private dialog: MatDialog,
     @Inject(MAT_DIALOG_DATA) public data: { book: any },
  private dialogRef: MatDialogRef<VerifyActionComponent>) { }
  
  async ngOnInit() {
  try {
    this.book = this.data.book
    console.log('this.data.book: ', this.data.book);
  } catch (error) {
    console.warn('this.book: ', this.book);
  }

  // if(!this.book){
  //   this.book = {
  //     title:'',price:0,released:'', score:0, id:null
  //   }
  // }
  }


  showMessage(message: string, type: string) {
    this.snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [type],
    });
  }

  async deleteBook(){
    try {
      const deleteResponse = 
      await fetch(environment.delete + this.book.id,{
        method: 'DELETE'
      })
      this.showMessage( 'Kirja poistettu onnistuneesti', 'successToast');
      console.log('deleteResponse: ', deleteResponse.status);
      this.dialog.closeAll();
    } catch (error) {
      this.showMessage( 'Kirjan poisto epäonnistui', 'errorToast');
    }
  }

  close() {
    this.dialogRef.close();
  }
}
