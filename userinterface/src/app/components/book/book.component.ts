import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatDialog } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { book } from 'src/app/models/book.model';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { VerifyActionComponent } from '../verify-action/verify-action.component';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
})
export class BookComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  public book: book = {
    title: '',
    price: null,
    released: '',
    score: null,
    id: '',
  };
  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: { book: any },
    private dialogRef: MatDialogRef<BookComponent>
  ) {}

  async ngOnInit() {
    // try {
    //   this.book = this.data.book;
    //   console.log('this.data.book: ', this.data.book);
    // } catch (error) {
    //   console.warn('this.book: ', this.book, error);
    // }
    try {
      this.book = this.data.book;

      const output = await fetch(environment.read + this.book.id, {
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
      });

      const outputJSON = await output.json();

      this.book = outputJSON;
      console.log('this.book: ', this.book);
    } catch (error) {
      console.warn('this.book: ', this.book, error);
    }

    if (!this.book) {
      this.book = {
        title: '',
        price: null,
        released: '',
        score: null,
        id: null,
      };
    }
  }

  async saveBook() {
    try {
      if (!this.book.title) {
        this.showMessage('Lisää kirjan nimi', 'errorToast');
        return;
      }
      if (this.book.score > 5) {
        this.book.score = 5;
      } else if (this.book.score < 0) {
        this.book.score = 0;
      }
      const requestBody = { book: this.book };

      if (!this.book.id) {
        try {
          const createResponse = await fetch(environment.post, {
            method: 'POST',
            body: JSON.stringify(requestBody),
            headers: {
              'Content-Type': 'application/json',
            },
          });
          this.showMessage('Kirja arvostelu luotu', 'successToast');
          this.dialogRef.close();
          return;
        } catch (error) {
          console.log('error: ', error);
          this.showMessage(
            'Kirja arvostelun luominen epäonnistui',
            'errorToast'
          );
        }
      } else if (this.book.id) {
        const updateResponse = await fetch(environment.put + this.book.id, {
          method: 'PUT',
          body: JSON.stringify(requestBody),
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
        });
      }
      this.dialogRef.close();

      this.showMessage('Kirja arvostelu muokattu', 'successToast');
    } catch (error) {
      this.showMessage('Kirja arvostelun muokkaus epäonnistui', 'errorToast');
    }
  }

  showMessage(message: string, type: string) {
    this.snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      panelClass: [type],
    });
  }

  async deleteBook() {
    try {
      const deleteResponse = await fetch(environment.delete + this.book.id, {
        method: 'DELETE',
      });
      this.showMessage('Kirja poistettu onnistuneesti', 'successToast');
      console.log('deleteResponse: ', deleteResponse.status);
      this.dialogRef.close();
    } catch (error) {
      this.showMessage('Kirjan poisto epäonnistui', 'errorToast');
    }
  }

  close() {
    this.dialogRef.close();
  }

  async verifyDeleteDialog(book: any) {
    console.log('opens DIALOG, book data =' + JSON.stringify(book));
    this.dialog
      .open(VerifyActionComponent, {
        autoFocus: true,
        data: {
          book: this.book,
        },
        width: '100%',
        maxWidth: 800,
      })
      .afterClosed()
      .subscribe(async () => {});
  }
}
