const functions = require("firebase-functions");
const formidable = require("formidable-serverless");
const multer = require("multer");
const { Storage } = require("@google-cloud/storage");
const { v4: uuidv4 } = require("uuid");
const admin = require("firebase-admin");
const express = require("express");
const cors = require("cors");
const app = express();
app.use(cors({ origin: true }));

const serviceAccount = require("./serviceAccountKey.json");

const firebaseConfig = {
  credential: admin.credential.cert(serviceAccount),
};

admin.initializeApp(firebaseConfig);

const db = admin.firestore();

// Use compression and helmet for security
const compression = require("compression");
const helmet = require("helmet");
app.use(helmet({ contentSecurityPolicy: true }));
app.use(compression());

// Rate limiter to limit requests from a single IP
const RateLimit = require("express-rate-limit");
const limiter = RateLimit({
  windowMs: 60 * 1000, // 1 minute
  max: 10, // maximum of 10 requests per minute
  message: "Too many requests from this IP, please try again after a minute",
  headers: true,
});
// Apply rate limiter to all requests
app.use(limiter);

const upload = multer({
  storage: multer.memoryStorage(),
  limits: { fileSize: 1024 * 1024 * 100 }, //100mb
});

exports.video = functions.https.onRequest((req, res) => {
  const form = new formidable.IncomingForm();
  return new Promise((resolve, reject) => {
    form.parse(req, async (err, fields, files) => {
      //todo if name field is not 'file' or empty apicall is broke so fix that
      let file = files[""] || files.file;

      if (!file) {
        reject(new Error("no file to upload, please choose a file."));
        return;
      }

      let filePath = file.path;
      console.log("File path: " + filePath);

      let storage = new Storage({
        keyFilename: "./serviceAccountKey.json",
      });

      let destination = "videos/" + file.name;

      let uuid = uuidv4();

      let response = await storage
        .bucket("testingapi-d7568.appspot.com")
        .upload(filePath, {
          // resumable: true,
          destination,
          contentType: file.type,
          metadata: {
            metadata: {
              firebaseStorageDownloadTokens: uuid,
            },
          },
        });

      const fullMediaLink = response[0].metadata.mediaLink + "";
      const mediaLinkPath = fullMediaLink.substring(
        0,
        fullMediaLink.lastIndexOf("/") + 1
      );
      const downloadUrl =
        mediaLinkPath +
        encodeURIComponent(response[0].name) +
        "?alt=media&token=" +
        uuid;

      console.log("downloadUrl", downloadUrl);

      resolve({ fileInfo: response[0].metadata, downloadUrl }); // Whole thing completed successfully.
    });
  })
    .then((response) => {
      res.status(200).json({ response });
      return null;
    })
    .catch((err) => {
      console.error("Error while parsing form: " + err);
      res.status(500).json({ error: err });
    });
});

// endpoints

// create
app.post("/api/create", (req, res) => {
  (async () => {
    try {
      const bookId = Math.floor(Math.random() * 99999999999999999999);
      req.body.id = bookId;
      req.body.book.id = bookId;
      const now = new Date();
      const formattedDate = now.toISOString().slice(0, 19).replace("T", " ");
      req.body.book.added = formattedDate;
      await db
        .collection("books")
        .doc("/" + bookId + "/")
        .create(req.body.book);
      return res.status(200).send({ status: "successfully created ", bookId });
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  })();
});

// read
app.get("/api/read/:book_id", (req, res) => {
  (async () => {
    try {
      const document = db.collection("books").doc(req.params.book_id);
      const item = await document.get();
      const response = item.data();
      return res.status(200).send(response);
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  })();
});

// read all
app.get("/api/read", (req, res) => {
  (async () => {
    try {
      const query = db.collection("books");
      const response = [];
      await query.get().then((querySnapshot) => {
        const docs = querySnapshot.docs;
        for (const doc of docs) {
          const selectedItem = {
            id: doc.id,
            book: doc.data(),
          };
          response.push(selectedItem);
        }
        return response;
      });
      return res.status(200).send(response);
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  })();
});

// update
app.put("/api/update/:book_id", (req, res) => {
  (async () => {
    try {
      const document = db.collection("books").doc(req.params.book_id);
      await document.update(req.body.book);
      return res.status(200).send({ status: "successfully updated" });
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  })();
});

// delete
app.delete("/api/delete/:book_id", (req, res) => {
  (async () => {
    try {
      const document = db.collection("books").doc(req.params.book_id);
      await document.delete();
      return res.status(200).send({ status: "successfully deleted" });
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  })();
});

// delete all
app.delete("/api/delete-all", (req, res) => {
  (async () => {
    try {
      const query = db.collection("books");
      console.log("query: ", query);
      await query.get().then((querySnapshot) => {
        const docs = querySnapshot.docs;
        console.log("docs: ", docs);
        for (const doc of docs) {
          const document = db.collection("books").doc(doc.id);
          document.delete();
        }
        return res.status(200).send({ status: "successfully deleted all" });
      });
    } catch (error) {
      console.log(error);
      return res.status(500).send(error);
    }
  })();
});

app.get("/hello", (req, res) => {
  res.send("Hello World!");
});

exports.app = functions.https.onRequest(app);
